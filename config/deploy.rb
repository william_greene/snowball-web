set :application, "SnowballFactory.com"
set :repository,  "git@github.com:seldo/Snowball-Web.git"

set :scm, :git
set :scm_user, 'webuser'
#set :scm_passphrase, 'g1td3pl0y'

set :user, 'root'
set :webuser, 'webuser' # this is my hack

role :web, "snowwebprod"                          # Your HTTP server, Apache/etc
role :app, "snowwebprod"                          # This may be the same as your `Web` server
role :db,  "snowwebprod", :primary => true # This is where Rails migrations will run

set :deploy_to, "/var/www/snowball"

default_run_options[:pty] = true # VITAL. Makes it ask for the password for git.
ssh_options[:user] = 'root' # maybe useful
ssh_options[:auth_methods] = "publickey" # VITAL. Makes it able to log into the AWS box.
#ssh_options[:verbose] = :debug # makes it noisy

# these are all useless
#ssh_options[:config] = false #appears to break stuff
#ssh_options[:keys] = %w(~/.ssh/snowball-web.pem) # seems pointless
#ssh_options[:port] = 8765 # not listening
#ssh_options[:forward_agent] = true # makes it pointlessly try agent



# If you are using Passenger mod_rails uncomment this:
# if you're still using the script/reapear helper you will need
# these http://github.com/rails/irs_process_scripts

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end