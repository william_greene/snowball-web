YUI().use('node-base', function(Y) {
	Y.on('domready', function() {
		var e1 = Y.one("#email-1");
		var e2 = Y.one("#email-2");
		var e3 = Y.one("#email-3");
		e2.set('innerHTML', "@" + e2.get('innerHTML'));
		e3.set('innerHTML', "." + e3.get('innerHTML'));
		
		var e = Y.one("#email");
		var eddress = e1.get('innerHTML') + e2.get('innerHTML') + e3.get('innerHTML');
		e.set('innerHTML', '<a href="mailto:' + eddress + '">' + eddress + '</a>');
	})
});
